<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;
use Tests\Smorken\PinAuth\TestbenchTestCase;

class LogoutControllerTest extends TestbenchTestCase
{
    use RefreshDatabase;

    #[Test]
    public function it_logouts_out_the_current_pin_user(): void
    {
        $user = PinUser::factory()->create();
        $this->actingAs($user, 'pin_user');
        $this->assertAuthenticated('pin_user');
        $this->post('/pin/logout')
            ->assertRedirect('/');
        $this->assertGuest();
    }
}

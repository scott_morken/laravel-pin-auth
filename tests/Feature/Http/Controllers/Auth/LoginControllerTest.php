<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;
use Tests\Smorken\PinAuth\TestbenchTestCase;

class LoginControllerTest extends TestbenchTestCase
{
    use RefreshDatabase;

    #[Test]
    public function it_fails_validation_without_a_password(): void
    {
        PinUser::factory()->create();
        $response = $this->post('/pin/login', ['password' => '']);
        $response->assertInvalid(['password' => 'The password field is required.']);
        $this->assertGuest();
    }

    #[Test]
    public function it_rate_limits_login_attempts(): void
    {
        $this->post('/pin/login', ['password' => '12345'])
            ->assertRedirectToRoute('pin.login');
        $this->post('/pin/login', ['password' => '12345'])
            ->assertRedirectToRoute('pin.login');
        $this->post('/pin/login', ['password' => '12345'])
            ->assertRedirectToRoute('pin.login');
        $this->post('/pin/login', ['password' => '12345'])
            ->assertRedirectToRoute('pin.login');
        $this->post('/pin/login', ['password' => '12345'])
            ->assertRedirectToRoute('pin.login');
        $response = $this->post('/pin/login', ['password' => '12345']);
        $response->assertInvalid(['password' => 'Too many login attempts. Please try again in']);
        $this->assertGuest();
    }

    #[Test]
    public function it_redirects_to_home_page_after_successful_login(): void
    {
        PinUser::factory()->create();
        $response = $this->post('/pin/login', ['password' => '12345678']);
        $response->assertRedirect('/');
        $this->assertAuthenticated('pin_user');
    }

    #[Test]
    public function it_shows_the_login_form(): void
    {
        $this->get('/pin/login')
            ->assertSee('input type="password"', false);
    }
}

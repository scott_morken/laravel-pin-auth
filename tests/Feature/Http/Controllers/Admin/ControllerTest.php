<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Feature\Http\Controllers\Admin;

use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\PinAuth\Admin\Contracts\Repositories\FilteredPinUsersRepository;
use Smorken\PinAuth\Admin\Contracts\Repositories\FindPinUserRepository;
use Smorken\PinAuth\Admin\Factories\PinUserRepositoryFactory;
use Smorken\PinAuth\Auth\Contracts\PinUserProvider;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\PinAuth\TestbenchTestCase;

class ControllerTest extends TestbenchTestCase
{
    use RefreshDatabase;

    #[Test]
    public function it_allows_an_admin_user(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $this->actingAs($user)
            ->get('/admin/pin-user')
            ->assertSuccessful();
    }

    #[Test]
    public function it_creates_a_pin_user(): void
    {
        $this->assertDatabaseCount('roles', 5);
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $this->followingRedirects()
            ->actingAs($user)
            ->post('/admin/pin-user', ['name' => 'Bizz Buzz', 'pin' => 'abcdef12', 'pin_confirmation' => 'abcdef12'])
            ->assertSuccessful()
            ->assertSee('Bizz Buzz');
        /** @var \Smorken\PinAuth\Auth\Contracts\PinUserProvider $p */
        $p = $this->app[PinUserProvider::class];
        $m = $p->retrieveByCredentials(['password' => 'abcdef12']);
        $this->assertEquals('Bizz Buzz', $m->name);
    }

    #[Test]
    public function it_creates_a_pin_user_with_an_external_id(): void
    {
        $this->assertDatabaseCount('roles', 5);
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $this->followingRedirects()
            ->actingAs($user)
            ->post('/admin/pin-user', [
                'name' => 'Bizz Buzz', 'pin' => 'abcdef12', 'pin_confirmation' => 'abcdef12', 'external_id' => '12345',
            ])
            ->assertSuccessful()
            ->assertSee('Bizz Buzz')
            ->assertSee('12345');
        /** @var \Smorken\PinAuth\Auth\Contracts\PinUserProvider $p */
        $p = $this->app[PinUserProvider::class];
        $m = $p->retrieveByCredentials(['password' => 'abcdef12']);
        $this->assertEquals('Bizz Buzz', $m->name);
        $this->assertEquals('12345', $m->external_id);
    }

    #[Test]
    public function it_deletes_a_pin_user(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $pu = PinUser::factory()->create(['pin' => '1234']);
        $this->actingAs($user)
            ->get('/admin/pin-user/'.$pu->id.'/confirm-delete')
            ->assertSee($pu->name)
            ->assertDontSee('cd88065a0d0d9006c1afc32fd53a3b9ca59b6890624b08a1e316cb19b2fa299c');
        $this->actingAs($user)
            ->delete('/admin/pin-user/'.$pu->id)
            ->assertRedirect('/admin/pin-user');
        $this->assertDatabaseMissing(new PinUser, ['id' => $pu->id]);
    }

    #[Test]
    public function it_fails_validation_on_user_creation(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $this->actingAs($user)
            ->post('/admin/pin-user')
            ->assertInvalid(['name' => 'The name field is required.', 'pin' => 'The pin field is required.']);
    }

    #[Test]
    public function it_fails_validation_without_confirmation(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $this->actingAs($user)
            ->post('/admin/pin-user', ['name' => 'Bizz Buzz', 'pin' => 'abcdef12'])
            ->assertInvalid(['pin' => 'The pin field confirmation does not match.']);
    }

    #[Test]
    public function it_is_unauthorized_for_a_nonadmin_user(): void
    {
        $user = new GenericUser(['id' => 99]);
        $this->actingAs($user)
            ->get('/admin/pin-user')
            ->assertForbidden();
    }

    #[Test]
    public function it_lists_pin_users_on_the_index(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $pu = PinUser::factory()->create();
        $this->actingAs($user)
            ->get('/admin/pin-user')
            ->assertSee($pu->name);
    }

    #[Test]
    public function it_updates_a_pin_user(): void
    {
        $user = new GenericUser(['id' => 1]);
        $this->addToRoleUsers(1, 2);
        $pu = PinUser::factory()->create(['name' => 'Bizz Buzz', 'pin' => 'abcdef12']);
        $this->actingAs($user)
            ->put('/admin/pin-user/'.$pu->id,
                ['name' => 'Bizz Buzz', 'pin' => '12345678', 'pin_confirmation' => '12345678'])
            ->assertRedirect('/admin/pin-user');
        $this->get('/admin/pin-user')
            ->assertSee('Bizz Buzz');
        /** @var \Smorken\PinAuth\Auth\Contracts\PinUserProvider $p */
        $p = $this->app[PinUserProvider::class];
        $m = $p->retrieveByCredentials(['password' => 'abcdef12']);
        $this->assertNull($m);
        $m = $p->retrieveByCredentials(['password' => '12345678']);
        $this->assertEquals('Bizz Buzz', $m->name);
    }

    protected function addToRoleUsers(int $userId, int $roleId): void
    {
        RoleUser::factory()->create(['user_id' => $userId, 'role_id' => $roleId]);
    }

    protected function tearDown(): void
    {
        /** @var \Smorken\PinAuth\Admin\Factories\PinUserRepositoryFactory $factory */
        $factory = $this->app->make(PinUserRepositoryFactory::class);
        ($factory->make(FilteredPinUsersRepository::class))->resetCacheHandler();
        ($factory->make(FindPinUserRepository::class))->resetCacheHandler();
        RoleUser::truncate();
        parent::tearDown();
    }
}

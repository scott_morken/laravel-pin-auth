<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\Test;
use Smorken\PinAuth\Auth\Contracts\PinUserProvider;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;
use Tests\Smorken\PinAuth\TestbenchTestCase;

class PinUserProviderTest extends TestbenchTestCase
{
    use RefreshDatabase;

    #[Test]
    public function it_fails_to_retrieve_a_user_by_incorrect_pin(): void
    {
        PinUser::factory()->create();
        /** @var \Smorken\PinAuth\Auth\Contracts\PinUserProvider $p */
        $p = $this->app[PinUserProvider::class];
        $m = $p->retrieveByCredentials(['password' => '1234567']);
        $this->assertNull($m);
    }

    #[Test]
    public function it_retrieves_user_by_pin(): void
    {
        PinUser::factory()->create();
        /** @var \Smorken\PinAuth\Auth\Contracts\PinUserProvider $p */
        $p = $this->app[PinUserProvider::class];
        $m = $p->retrieveByCredentials(['password' => '12345678']);
        $this->assertEquals('Foo Bar', $m->name);
    }
}

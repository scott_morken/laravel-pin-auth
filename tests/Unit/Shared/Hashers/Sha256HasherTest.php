<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Unit\Shared\Hashers;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\PinAuth\Shared\Hashers\Sha256Hasher;
use Tests\Smorken\PinAuth\Stubs\OriginalSha256;

class Sha256HasherTest extends TestCase
{
    #[Test]
    public function it_should_match_the_original_hasher(): void
    {
        $salt = 'salt and pepper';
        $hmac = 'base64:NoTrAnDoM0WpUIISaRTWktTJ/Qa0CYi1yOb0wQ4bcUA=';
        $original = new OriginalSha256($salt, $hmac);
        $sut = new Sha256Hasher($salt, $hmac);
        $this->assertEquals($original->get('foo bar'), $sut->make('foo bar'));
    }
}

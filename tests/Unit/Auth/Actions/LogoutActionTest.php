<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Unit\Auth\Actions;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction;

class LogoutActionTest extends TestCase
{
    protected ?Factory $authFactory = null;

    #[Test]
    public function it_logs_out_guard_and_clears_session(): void
    {
        $guard = m::mock(Guard::class);
        $this->getAuthFactory()->expects()
            ->guard('pin_users')
            ->andReturns($guard);
        $guard->expects()->logout();
        $session = m::mock(Session::class);
        $session->expects()->invalidate();
        $session->expects()->regenerateToken();
        $request = new Request;
        $request->setLaravelSession($session);
        $sut = $this->getSut();
        $sut($request);
        $this->assertTrue(true);
    }

    protected function getAuthFactory(): Factory
    {
        return $this->authFactory ?? $this->authFactory = m::mock(Factory::class);
    }

    protected function getSut(): LogoutAction
    {
        return new \Smorken\PinAuth\Auth\Actions\LogoutAction($this->getAuthFactory(), 'pin_users');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}

<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Unit\Auth\Actions;

use Illuminate\Cache\RateLimiter;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\PinAuth\Auth\Contracts\Actions\LoginAction;
use Smorken\PinAuth\Auth\Contracts\Key\PinKeyProvider;
use Tests\Smorken\PinAuth\Concerns\WithApplication;

class LoginActionTest extends TestCase
{
    use WithApplication;

    protected ?\Illuminate\Contracts\Auth\Factory $authFactory = null;

    protected ?PinKeyProvider $pinKeyProvider = null;

    protected ?RateLimiter $rateLimiter = null;

    #[Test]
    public function it_can_succeed_on_a_login_attempt(): void
    {
        $sut = $this->getSut();
        $this->getPinKeyProvider()->allows()
            ->__invoke()
            ->andReturns('testkey');
        $this->getRateLimiter()->expects()
            ->tooManyAttempts('testkey127.0.0.1', 5)
            ->andReturns(false);
        $guard = m::mock(Guard::class);
        $this->getAuthFactory()->expects()
            ->guard('pin_users')
            ->andReturns($guard);
        $guard->expects()
            ->attempt(['password' => '12345678'])
            ->andReturn(true);
        $this->getRateLimiter()->expects()
            ->clear('testkey127.0.0.1');
        $session = m::mock(Session::class);
        $session->expects()->regenerate();
        $request = (new Request([], [], [], [], [], ['REMOTE_ADDR' => '127.0.0.1']))->merge(['password' => '12345678']);
        $request->setLaravelSession($session);
        $this->assertTrue($sut($request));
    }

    #[Test]
    public function it_fails_validation_without_password(): void
    {
        $sut = $this->getSut();
        $request = new Request([], [], [], [], [], ['REMOTE_ADDR' => '127.0.0.1']);
        $this->expectException(ValidationException::class);
        $sut($request);
    }

    #[Test]
    public function it_hits_the_rate_limiter_on_attempt_failure(): void
    {
        $sut = $this->getSut();
        $this->getPinKeyProvider()->allows()
            ->__invoke()
            ->andReturns('testkey');
        $this->getRateLimiter()->expects()
            ->tooManyAttempts('testkey127.0.0.1', 5)
            ->andReturns(false);
        $guard = m::mock(Guard::class);
        $this->getAuthFactory()->expects()
            ->guard('pin_users')
            ->andReturns($guard);
        $guard->expects()
            ->attempt(['password' => '12345678'])
            ->andReturn(false);
        $this->getRateLimiter()->expects()
            ->hit('testkey127.0.0.1');

        $request = (new Request([], [], [], [], [], ['REMOTE_ADDR' => '127.0.0.1']))->merge(['password' => '12345678']);
        $this->assertFalse($sut($request));
    }

    protected function getAuthFactory(): \Illuminate\Contracts\Auth\Factory
    {
        return $this->authFactory ?? $this->authFactory = m::mock(\Illuminate\Contracts\Auth\Factory::class);
    }

    protected function getPinKeyProvider(): PinKeyProvider
    {
        return $this->pinKeyProvider ?? $this->pinKeyProvider = m::mock(PinKeyProvider::class);
    }

    protected function getRateLimiter(): RateLimiter
    {
        return $this->rateLimiter ?? $this->rateLimiter = m::mock(RateLimiter::class);
    }

    protected function getSut(): LoginAction
    {
        return new \Smorken\PinAuth\Auth\Actions\LoginAction(
            $this->getAuthFactory(),
            $this->getPinKeyProvider(),
            $this->getRateLimiter(),
            'pin_users'
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->getApp();
        App::bind('translator', fn () => new Translator(new ArrayLoader, 'en'));
        \Smorken\PinAuth\Auth\Actions\LoginAction::setValidationFactory(new Factory(new Translator(new ArrayLoader,
            'en')));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}

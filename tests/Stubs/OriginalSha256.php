<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Stubs;

class OriginalSha256
{
    public function __construct(protected string $salt, protected string $hmac) {}

    public function compare(string $str, string $hashed): bool
    {
        $comp = $this->get($str);

        return hash_equals($comp, $hashed);
    }

    public function get(string $str): string
    {
        if (! $str) {
            throw new \InvalidArgumentException('String must exist to hash.');
        }
        if (! $this->salt) {
            throw new \InvalidArgumentException('No salt set.');
        }
        if (! $this->hmac) {
            throw new \InvalidArgumentException('HMAC key is not set.');
        }

        return hash_hmac('sha256', $str.$this->salt, $this->hmac);
    }
}

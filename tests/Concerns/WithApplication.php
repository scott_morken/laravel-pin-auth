<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth\Concerns;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Facade;

trait WithApplication
{
    protected Application|Container|null $app = null;

    protected function getApp(): Application
    {
        if (! $this->app) {
            $app = new \Illuminate\Foundation\Application(realpath(__DIR__.'/../../'));
            \Illuminate\Container\Container::setInstance($app);
            Facade::clearResolvedInstances();
            Facade::setFacadeApplication($app);
            $this->app = $app;
        }

        return $this->app;
    }
}

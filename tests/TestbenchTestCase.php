<?php

declare(strict_types=1);

namespace Tests\Smorken\PinAuth;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Contracts\Config\Repository;
use Orchestra\Testbench\TestCase;
use Smorken\Domain\ActionServiceProvider;
use Smorken\Domain\DomainServiceProvider;
use Smorken\Domain\ModelServiceProvider;
use Smorken\Domain\RepositoryServiceProvider;
use Smorken\PinAuth\ServiceProvider;
use Smorken\Roles\Support\DefineGates;

class TestbenchTestCase extends TestCase
{
    protected bool $seed = true;

    protected string $seeder = RoleSeeder::class;

    protected function defineEnvironment($app): void
    {
        tap($app['config'], function (Repository $config) {
            $config->set('sm-pin-auth.layout', 'sm-pin-auth::layout');
            $config->set('auth.providers.pin_user_provider', ['driver' => 'pin_users']);
            $config->set('auth.guards.pin_user', ['driver' => 'session', 'provider' => 'pin_user_provider']);
        });
    }

    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
            DomainServiceProvider::class,
            ActionServiceProvider::class,
            RepositoryServiceProvider::class,
            ModelServiceProvider::class,
            \Smorken\CacheAssist\ServiceProvider::class,
            \Smorken\Components\ServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        DefineGates::define($this->app, true);
    }

    protected function tearDown(): void
    {
        DefineGates::reset();
        parent::tearDown();
    }
}

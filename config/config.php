<?php

declare(strict_types=1);

return [
    'layout' => env('PINAUTH_LAYOUT', 'layouts.app'),
    'debug' => env('APP_DEBUG', false),
    'guard_name' => env('PINAUTH_GUARD_NAME', 'pin_user'),
    'provider_name' => env('PINAUTH_PROVIDER_NAME', 'pin_users'),
    'cache_ttl' => 60 * 60,
    'load_admin_routes' => env('PINAUTH_ADMIN_ROUTES', true),
    'load_auth_routes' => env('PINAUTH_AUTH_ROUTES', true),
    'admin_route_prefix' => 'admin',
    'admin_route_middleware' => ['web', 'auth', 'can:role-admin'],
    'auth_route_prefix' => 'pin',
    'auth_route_middleware' => ['web'],
    'hasher_salt' => env('PINAUTH_HASHER_SALT'),
    'external_id_rules' => env('PINAUTH_EXTERNAL_ID_RULES', 'nullable|string|max:64'),
    'controllers' => [
        'login' => \Smorken\PinAuth\Http\Controllers\Auth\LoginController::class,
        'do_login' => \Smorken\PinAuth\Http\Controllers\Auth\DoLoginController::class,
        'logout' => \Smorken\PinAuth\Http\Controllers\Auth\LogoutController::class,
        'admin' => \Smorken\PinAuth\Http\Controllers\Admin\Controller::class,
    ],
    'models' => [
        \Smorken\PinAuth\Shared\Contracts\Models\PinUser::class => \Smorken\PinAuth\Shared\Models\Eloquent\PinUser::class,
    ],
    'repositories' => [
        \Smorken\PinAuth\Admin\Contracts\Repositories\FilteredPinUsersRepository::class => \Smorken\PinAuth\Admin\Repositories\FilteredPinUsersRepository::class,
        \Smorken\PinAuth\Admin\Contracts\Repositories\FindPinUserRepository::class => \Smorken\PinAuth\Admin\Repositories\FindPinUserRepository::class,
    ],
    'actions' => [
        \Smorken\PinAuth\Admin\Contracts\Actions\DeletePinUserAction::class => \Smorken\PinAuth\Admin\Actions\DeletePinUserAction::class,
        \Smorken\PinAuth\Admin\Contracts\Actions\UpsertPinUserAction::class => \Smorken\PinAuth\Admin\Actions\UpsertPinUserAction::class,
    ],
];

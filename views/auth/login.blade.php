@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-pin-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <form class="form-signin" method="post" action="{{ route('pin.doLogin') }}">
        @csrf
        <input type="password" name="password" id="password" value="" placeholder="pin" maxlength="32"
               style="border-top-left-radius: 0.5em; border-top-right-radius: 0.5em"
               class="form-control" autocomplete="off" required autofocus aria-label="Pin"/>
        <button type="submit" name="login" class="btn btn-primary btn-block btn-lg w-100">Login</button>
        <div style="margin: .5em;">
            Only active students, currently employed staff, and designated 3rd parties are authorized
            to access MCCCD and Phoenix College resources.
        </div>
    </form>
</x-dynamic-component>

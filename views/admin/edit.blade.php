@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-pin-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.edit
        title="Pin Users Administration"
        form-view="sm-pin-auth::admin._form"
        :viewModel="$viewModel"></x-smc::resource.edit>
</x-dynamic-component>

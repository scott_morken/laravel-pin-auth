@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-pin-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.show-rows title="Pin Users Administration"
                               :limitAttributes="['id', 'name', 'created_at', 'updated_at']"
                               :viewModel="$viewModel"></x-smc::resource.show-rows>
</x-dynamic-component>

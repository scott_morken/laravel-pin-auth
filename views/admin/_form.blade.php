<?php
use Smorken\Components\Helpers\Model;

/**
 * @var \Smorken\PinAuth\Shared\Contracts\Models\PinUser $model
 */
?>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'name'))
    <x-smc::input.label :model="$m">Name</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    <x-smc::input.label for="pin">Pin</x-smc::input.label>
    <x-smc::input type="password" name="pin" autocomplete="off"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    <x-smc::input.label for="pin_confirmation">Confirm Pin</x-smc::input.label>
    <x-smc::input type="password" name="pin_confirmation" autocomplete="off"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'external_id'))
    <x-smc::input.label :model="$m">SIS ID (3...)</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>



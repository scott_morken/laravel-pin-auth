<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\FormQsFilter $helper
 */
?>
<x-smc::form.qs-filter :filter="$filter">
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="filter-name" :visible="false">Name</x-smc::input.label>
            <x-smc::input name="filter[name]" class="{{ $helper->getClasses('name') }}"
                          :value="$helper->getValue('name')" placeholder="Name like..."/>
        </x-smc::input.group>
        <x-smc::input.group>
            <x-smc::input.label for="filter-externalid" :visible="false">External ID</x-smc::input.label>
            <x-smc::input name="filter[externalId]" class="{{ $helper->getClasses('externalId') }}"
                          :value="$helper->getValue('externalId')" placeholder="SIS ID (3...)"/>
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.qs-filter>

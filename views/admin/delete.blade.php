@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-pin-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.confirm-delete title="Pin Users Administration"
                                    :limitAttributes="['id', 'name', 'created_at', 'updated_at',]"
                                    :viewModel="$viewModel"></x-smc::resource.confirm-delete>
</x-dynamic-component>

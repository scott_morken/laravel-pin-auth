<?php
use \Illuminate\Support\Facades\Config;
use Illuminate\Support\HtmlString;
use Smorken\Components\Helpers\Action;

/**
 * @var \Smorken\Domain\ViewModels\Contracts\FilteredViewModel $viewModel
 * @var \Smorken\PinAuth\Shared\Contracts\Models\PinUser $model
 */
$layoutComponent = Config::get('sm-pin-auth.layout', 'layouts.app');
$filter = $viewModel->filter();
?>
<x-dynamic-component :component="$layoutComponent">
    <x-smc::title>Pin Users Administration</x-smc::title>
    <div class="clearfix">
        @php($params = $params ?? ($filter->toArray() ?? []))
        <div class="btn-group float-md-end" role="group" aria-label="Create options">
            <x-smc::button.outline-success
                    :href="new HtmlString(action(Action::make($controller, 'create'), $params))"
                    class="btn-sm"
            >Create
            </x-smc::button.outline-success>
        </div>
    </div>
    @include('sm-pin-auth::admin._filter_form')
    @if ($viewModel->models() && count($viewModel->models()))
        <x-smc::table>
            <x-slot:head>
                <x-smc::table.heading>ID</x-smc::table.heading>
                <x-smc::table.heading>Name</x-smc::table.heading>
                <x-smc::table.heading>External ID</x-smc::table.heading>
                <x-smc::table.heading>Updated</x-smc::table.heading>
                <x-smc::table.heading>&nbsp;</x-smc::table.heading>
            </x-slot:head>
            <x-slot:body>
                @foreach ($viewModel->models() as $model)
                    @php($params = [...[$model->getKeyName() => $model->getKey()], ...$filter->toArray()])
                    <x-smc::table.row :model="$model">
                        <x-smc::table.cell>
                            <x-smc::resource.action.show
                                    title="View {{ $model->getKey() }}"
                                    :params="$params">{{ $model->getKey() }}</x-smc::resource.action.show>
                        </x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->name }}</x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->external_id }}</x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->updated_at->diffForHumans() }}</x-smc::table.cell>
                        <x-smc::table.cell>
                            <x-smc::resource.action.edit-confirm-delete
                                    :params="$params"></x-smc::resource.action.edit-confirm-delete>
                        </x-smc::table.cell>
                    </x-smc::table.row>
                @endforeach
            </x-slot:body>
        </x-smc::table>
        <x-smc::paginate.models :models="$viewModel->models()" :filter="$filter"></x-smc::paginate.models>
    @else
        <div class="text-muted">No records found.</div>
    @endif
</x-dynamic-component>

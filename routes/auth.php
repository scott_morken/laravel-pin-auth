<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\PinAuth\Http\Controllers\Auth\LoginController;
use Smorken\PinAuth\Http\Controllers\Auth\LogoutController;

Route::group([
    'prefix' => Config::get('sm-pin-auth.auth_route_prefix', 'pin'),
    'middleware' => Config::get('sm-pin-auth.auth_route_middleware', ['web']),
], function () {
    $loginController = Config::get('sm-pin-auth.controllers.login', LoginController::class);
    Route::get('login', $loginController)->name('pin.login');

    $doLoginController = Config::get('sm-pin-auth.controller.do_login', \Smorken\PinAuth\Http\Controllers\Auth\DoLoginController::class);
    Route::post('login', $doLoginController)->name('pin.doLogin');

    $logoutController = Config::get('sm-pin-auth.controllers.logout', LogoutController::class);
    Route::post('logout', $logoutController)->name('pin.logout');
});

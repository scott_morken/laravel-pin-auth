<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\PinAuth\Http\Controllers\Admin\Controller;

Route::middleware(Config::get('sm-pin-auth.admin_route_middleware', ['web', 'auth', 'can:role-admin']))
    ->prefix(Config::get('sm-pin-auth.admin_route_prefix', 'admin'))
    ->group(function () {
        $controller = Config::get('sm-pin-auth.controllers.admin', Controller::class);
        Route::get('pin-user/{id}/confirm-delete', [$controller, 'confirmDelete']);
        Route::resource('pin-user', $controller)
            ->parameters(['pin-user' => 'id']);
    });

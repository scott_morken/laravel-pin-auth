<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Smorken\PinAuth\Admin\Contracts\Actions\DeletePinUserAction;
use Smorken\PinAuth\Admin\Contracts\Actions\UpsertPinUserAction;

/**
 * @extends ActionFactory<\Smorken\PinAuth\Shared\Models\Eloquent\PinUser>
 */
class PinUserActionFactory extends ActionFactory
{
    protected array $handlers = [
        'delete' => DeletePinUserAction::class,
        'upsert' => UpsertPinUserAction::class,
    ];
}

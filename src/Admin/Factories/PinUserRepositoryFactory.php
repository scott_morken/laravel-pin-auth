<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Factories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\PinAuth\Admin\Contracts\Repositories\FilteredPinUsersRepository;
use Smorken\PinAuth\Admin\Contracts\Repositories\FindPinUserRepository;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @extends RepositoryFactory<\Smorken\PinAuth\Shared\Models\Eloquent\PinUser>
 */
class PinUserRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindPinUserRepository::class,
        'filtered' => FilteredPinUsersRepository::class,
    ];

    public function emptyModel(): PinUser
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): Paginator|Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): ?PinUser
    {
        return $this->handlerForFind($id, $throw);
    }
}

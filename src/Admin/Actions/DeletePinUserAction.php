<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Actions;

use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;

class DeletePinUserAction extends EloquentDeleteAction implements \Smorken\PinAuth\Admin\Contracts\Actions\DeletePinUserAction
{
    public function __construct(PinUser $model)
    {
        parent::__construct($model);
    }
}

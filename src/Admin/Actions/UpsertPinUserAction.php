<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Actions;

use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\PinAuth\Admin\Validation\RuleProviders\PinUserRules;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;

class UpsertPinUserAction extends EloquentUpsertAction implements \Smorken\PinAuth\Admin\Contracts\Actions\UpsertPinUserAction
{
    public $rulesProvider = PinUserRules::class;

    public function __construct(
        PinUser $model
    ) {
        parent::__construct($model);
    }
}

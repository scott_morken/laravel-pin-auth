<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Validation\RuleProviders;

class PinUserRules
{
    public static string $externalIdRules = 'nullable|string|max:64';

    public static function rules(array $overrides = []): array
    {
        $rules = [
            'name' => 'required|max:64',
            'pin' => 'required|string|min:4|max:64|confirmed',
        ];
        if (self::$externalIdRules) {
            $rules['external_id'] = self::$externalIdRules;
        }

        return [...$rules, ...$overrides];
    }
}

<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;

class FindPinUserRepository extends EloquentRetrieveRepository implements \Smorken\PinAuth\Admin\Contracts\Repositories\FindPinUserRepository
{
    public function __construct(PinUser $model)
    {
        parent::__construct($model);
    }
}

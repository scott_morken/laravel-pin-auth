<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;

class FilteredPinUsersRepository extends EloquentFilteredRepository implements \Smorken\PinAuth\Admin\Contracts\Repositories\FilteredPinUsersRepository
{
    public function __construct(PinUser $model)
    {
        parent::__construct($model);
    }
}

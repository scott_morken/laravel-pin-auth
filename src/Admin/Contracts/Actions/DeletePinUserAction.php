<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface DeletePinUserAction extends DeleteAction {}

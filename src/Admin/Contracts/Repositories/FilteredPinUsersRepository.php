<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredPinUsersRepository extends FilteredRepository {}

<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\Controller;
use Smorken\PinAuth\Auth\Contracts\Actions\LoginAction;
use Smorken\PinAuth\Http\Controllers\Auth\Concerns\HasRedirectPath;

class DoLoginController extends Controller
{
    use HasRedirectPath;

    public function __construct(
        protected LoginAction $loginAction
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): JsonResponse|RedirectResponse
    {
        if (($this->loginAction)($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function flashMessages(Request $request): void
    {
        if ($request->wantsJson()) {
            return;
        }
        foreach ($this->loginAction->getMessages()->toArray() as $key => $messages) {
            foreach ($messages as $i => $message) {
                $request->session()->flash($key.':'.$i, $message);
            }
        }
    }

    protected function sendFailedLoginResponse(Request $request): JsonResponse|RedirectResponse
    {
        $this->flashMessages($request);

        return $request->wantsJson()
            ? new JsonResponse(['errors' => $this->loginAction->getMessages()->toArray()], 401)
            : Redirect::route('pin.login');
    }

    protected function sendLoginResponse(Request $request): JsonResponse|RedirectResponse
    {
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : Redirect::intended($this->redirectPath($request));
    }
}

<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Auth\Concerns;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

trait HasRedirectPath
{
    protected function redirectPath(Request $request): string
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($request);
        }

        if (property_exists($this, 'redirectTo')) {
            return $this->redirectTo;
        }

        if (class_exists(RouteServiceProvider::class)) {
            return RouteServiceProvider::HOME;
        }

        return '/';
    }
}

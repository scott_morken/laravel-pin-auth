<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Auth;

use Illuminate\Contracts\View\View;
use Smorken\Controller\View\Controller;

class LoginController extends Controller
{
    protected string $baseView = 'sm-pin-auth::auth';

    public function __invoke(): View
    {
        return $this->makeView($this->getViewName('login'));
    }
}

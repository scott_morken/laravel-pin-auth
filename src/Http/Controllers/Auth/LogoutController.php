<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\Controller;
use Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction;
use Smorken\PinAuth\Http\Controllers\Auth\Concerns\HasRedirectPath;

class LogoutController extends Controller
{
    use HasRedirectPath;

    public function __construct(
        protected LogoutAction $logoutAction
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): JsonResponse|RedirectResponse
    {
        ($this->logoutAction)($request);

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : Redirect::to($this->redirectPath($request));
    }
}

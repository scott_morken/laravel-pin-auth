<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\PinAuth\Admin\Factories\PinUserActionFactory;
use Smorken\PinAuth\Admin\Factories\PinUserRepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;

class Controller extends CrudController implements WithActionFactory, WithRepositoryFactory
{
    protected string $baseView = 'sm-pin-auth::admin';

    public function __construct(
        PinUserActionFactory $actionFactory,
        PinUserRepositoryFactory $repositoryFactory
    ) {
        parent::__construct();
        $this->actionFactory = $actionFactory;
        $this->repositoryFactory = $repositoryFactory;
    }

    protected function getFilterFromRequest(Request $request): QueryStringFilter
    {
        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setFilters(['name', 'externalId'])
            ->addKeyValue('page');
    }
}

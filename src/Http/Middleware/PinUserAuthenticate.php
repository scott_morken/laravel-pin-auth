<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;

class PinUserAuthenticate extends Authenticate
{
    public function __construct(Factory $auth, protected string $guardName)
    {
        parent::__construct($auth);
    }

    protected function authenticate($request, array $guards)
    {
        $guards = [$this->guardName, ...$guards];
        parent::authenticate($request, $guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('pin.login');
    }
}

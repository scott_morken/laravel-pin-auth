<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction;

class LogoutPinUserOnly
{
    public function __construct(protected LogoutAction $action) {}

    public function handle(Request $request, Closure $next)
    {
        $this->action->logoutGuardOnly($request);

        return $next($request);
    }
}

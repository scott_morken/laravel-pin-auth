<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Validation\RuleProviders;

class LoginRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'password' => ['required', 'string', 'max:64'],
            ...$overrides,
        ];
    }
}

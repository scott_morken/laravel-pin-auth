<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Database\Eloquent\Model;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;

class PinUserProvider extends EloquentUserProvider implements \Smorken\PinAuth\Auth\Contracts\PinUserProvider
{
    public function createModel(): Model
    {
        $m = $this->model;
        if (is_string($this->model)) {
            $m = new $m;
        }

        /** @var Model $m */
        return $m->newInstance();
    }

    public function retrieveByCredentials(array $credentials): ?PinUser
    {
        $pin = $credentials['password'] ?? null;

        if (empty($pin)) {
            return null;
        }

        $hashedPin = $this->hasher->make($pin);
        // @phpstan-ignore method.notFound
        $query = $this->newModelQuery()->pinIs($hashedPin);

        return $query->first();
    }

    public function retrieveByToken($identifier, $token): ?PinUser
    {
        return null;
    }

    public function updateRememberToken(UserContract $user, $token): void {}
}

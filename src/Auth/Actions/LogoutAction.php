<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Actions;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Smorken\Domain\Actions\Action;

class LogoutAction extends Action implements \Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction
{
    public function __construct(
        protected Factory $authFactory,
        protected string $guardName
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): void
    {
        $this->getGuard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }

    public function logoutGuardOnly(Request $request): void
    {
        if ($this->getGuard()->check()) {
            $this->getGuard()->logout();
            $request->session()->remove($this->guardName);
        }
    }

    protected function getGuard(): Guard
    {
        return $this->authFactory->guard($this->guardName);
    }
}

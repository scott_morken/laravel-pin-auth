<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Actions;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Smorken\Domain\Actions\Action;
use Smorken\Domain\Actions\Concerns\HasValidationFactory;
use Smorken\PinAuth\Auth\Contracts\Key\PinKeyProvider;
use Smorken\PinAuth\Auth\Validation\RuleProviders\LoginRules;

class LoginAction extends Action implements \Smorken\PinAuth\Auth\Contracts\Actions\LoginAction
{
    use HasValidationFactory;

    protected string $rulesProvider = LoginRules::class;

    public function __construct(
        protected Factory $authFactory,
        protected PinKeyProvider $pinKeyProvider,
        protected RateLimiter $rateLimiter,
        protected string $guardName
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): bool
    {
        $this->validate($request->only('password'), $this->getRules());
        $this->ensureIsNotRateLimited($request);
        if (! $this->authFactory->guard($this->guardName)->attempt($request->only('password'))) {
            $this->messages->add('flash:danger', trans('auth.failed'));
            $this->rateLimiter->hit($this->throttleKey($request));

            return false;
        }
        $this->rateLimiter->clear($this->throttleKey($request));
        $request->session()->regenerate();

        return true;
    }

    protected function ensureIsNotRateLimited(Request $request): void
    {
        if (! $this->rateLimiter->tooManyAttempts($this->throttleKey($request), 5)) {
            return;
        }
        Event::dispatch(new Lockout($request));
        $seconds = $this->rateLimiter->availableIn($this->throttleKey($request));

        throw ValidationException::withMessages([
            'password' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    protected function throttleKey(Request $request): string
    {
        return Str::transliterate(($this->pinKeyProvider)().$request->ip());
    }
}

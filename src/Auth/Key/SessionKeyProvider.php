<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Key;

use Illuminate\Session\SessionManager;
use Smorken\PinAuth\Auth\Contracts\Key\PinKeyProvider;

class SessionKeyProvider implements PinKeyProvider
{
    public function __construct(protected SessionManager $session, protected string $key = 'pin_user_key') {}

    public function __invoke(): string
    {
        $currentKey = $this->session->get($this->key);
        if ($currentKey) {
            return $currentKey;
        }
        $key = $this->generateKey();
        $this->session->put($this->key, $key);

        return $key;
    }

    protected function generateKey(): string
    {
        return bin2hex(random_bytes(16));
    }
}

<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Contracts\Actions;

use Illuminate\Http\Request;
use Smorken\Domain\Actions\Contracts\Action;

interface LoginAction extends Action
{
    public function __invoke(Request $request): bool;
}

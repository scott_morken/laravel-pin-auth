<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Contracts\Key;

interface PinKeyProvider
{
    public function __invoke(): string;
}

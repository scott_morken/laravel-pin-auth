<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Auth\Contracts;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Database\Eloquent\Model;

interface PinUserProvider extends UserProvider
{
    public function createModel(): Model;
}

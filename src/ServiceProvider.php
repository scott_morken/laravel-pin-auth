<?php

declare(strict_types=1);

namespace Smorken\PinAuth;

use Illuminate\Cache\RateLimiter;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\Auth;
use Smorken\PinAuth\Admin\Validation\RuleProviders\PinUserRules;
use Smorken\PinAuth\Auth\Actions\LoginAction;
use Smorken\PinAuth\Auth\Contracts\Actions\LogoutAction;
use Smorken\PinAuth\Auth\Contracts\Key\PinKeyProvider;
use Smorken\PinAuth\Auth\Key\SessionKeyProvider;
use Smorken\PinAuth\Auth\PinUserProvider;
use Smorken\PinAuth\Http\Middleware\PinUserAuthenticate;
use Smorken\PinAuth\Shared\Contracts\Hasher;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;
use Smorken\PinAuth\Shared\Hashers\Sha256Hasher;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadConfig();
        $this->loadRoutes();
        $this->loadViews();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->handlePublishing();
        $this->registerAuthProvider();
        \Smorken\PinAuth\Shared\Models\Eloquent\PinUser::setHasherResolver(fn () => $this->app[Hasher::class]);
        LoginAction::setValidationFactory($this->app[Factory::class]);
        $this->setExternalIdRules();
    }

    public function register(): void
    {
        $this->registerActions();
        $this->registerRepositories();
        $this->registerModels();
        $this->registerPinUserProvider();
        $this->registerPinKeyProvider();
        $this->registerHasher();
        $this->registerLoginAction();
        $this->registerLogoutAction();
        $this->registerAuthMiddleware();
        $this->registerAuthMiddlewareAlias();
    }

    protected function handlePublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('sm-pin-auth.php'),
        ], 'pin-auth-config');
        $this->publishes([
            __DIR__.'/../views' => resource_path('views/vendor/sm-pin-auth'),
        ], 'pin-auth-views');
    }

    protected function loadConfig(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'sm-pin-auth'
        );
    }

    protected function loadRoutes(): void
    {
        if ($this->app['config']->get('sm-pin-auth.load_admin_routes', false)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
        }
        if ($this->app['config']->get('sm-pin-auth.load_auth_routes', false)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/auth.php');
        }
    }

    protected function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-pin-auth');
    }

    protected function registerActions(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-pin-auth.actions', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerAuthMiddleware(): void
    {
        $this->app->scoped(PinUserAuthenticate::class, fn (Application $app) => new PinUserAuthenticate(
            $app[\Illuminate\Contracts\Auth\Factory::class],
            $app['config']->get('sm-pin-auth.guard_name', 'pin_user')));
    }

    protected function registerAuthMiddlewareAlias(): void
    {
        $this->app['router']->aliasMiddleware('pin.auth', PinUserAuthenticate::class);
    }

    protected function registerAuthProvider(): void
    {
        $name = $this->app['config']->get('sm-pin-auth.provider_name', 'pin_users');
        Auth::provider($name,
            fn (Application $app, array $config) => $app[\Smorken\PinAuth\Auth\Contracts\PinUserProvider::class]);
    }

    protected function registerHasher(): void
    {
        $this->app->scoped(Hasher::class, function (Application $app) {
            $salt = $app['config']->get('sm-pin-auth.hasher_salt');
            $key = $app['config']->get('app.key');

            return new Sha256Hasher($salt, $key);
        });
    }

    protected function registerLoginAction(): void
    {
        $this->app->scoped(\Smorken\PinAuth\Auth\Contracts\Actions\LoginAction::class,
            fn (Application $app) => new LoginAction(
                $app[\Illuminate\Contracts\Auth\Factory::class],
                $app[PinKeyProvider::class],
                $app[RateLimiter::class],
                $app['config']->get('sm-pin-auth.guard_name', 'pin_user')
            ));
    }

    protected function registerLogoutAction(): void
    {
        $this->app->scoped(LogoutAction::class, fn (Application $app) => new \Smorken\PinAuth\Auth\Actions\LogoutAction(
            $app[\Illuminate\Contracts\Auth\Factory::class],
            $app['config']->get('sm-pin-auth.guard_name', 'pin_user')
        ));
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-pin-auth.models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerPinKeyProvider(): void
    {
        $this->app->scoped(PinKeyProvider::class, fn (Application $app) => new SessionKeyProvider($app['session']));
    }

    protected function registerPinUserProvider(): void
    {
        $this->app->scoped(\Smorken\PinAuth\Auth\Contracts\PinUserProvider::class, function (Application $app) {
            $m = $app[PinUser::class];

            return new PinUserProvider($app[Hasher::class], $m);
        });
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-pin-auth.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function setExternalIdRules(): void
    {
        $rules = $this->app['config']->get('sm-pin-auth.external_id_rules');
        PinUserRules::$externalIdRules = $rules;
    }
}

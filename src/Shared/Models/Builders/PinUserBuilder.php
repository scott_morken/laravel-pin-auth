<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Models\Builders;

use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\PinAuth\Shared\Models\Eloquent\PinUser
 *
 * @extends Builder<TModel>
 */
class PinUserBuilder extends Builder
{
    use WithQueryStringFilter;

    public function defaultOrder(): self
    {
        /** @var $this */
        return $this->orderBy('name');
    }

    public function externalIdIs(string|int $externalId): self
    {
        return $this->where('external_id', '=', $externalId);
    }

    public function nameLike(string $name): self
    {
        /** @var $this */
        return $this->where('name', 'LIKE', '%'.$name.'%');
    }

    public function pinIs(string $hash): self
    {
        /** @var $this */
        return $this->where('pin', '=', $hash);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('name', 'nameLike'),
            new FilterHandler('externalId', 'externalIdIs'),
        ];
    }
}

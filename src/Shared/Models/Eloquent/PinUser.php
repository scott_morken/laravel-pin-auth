<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Models\Eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Eloquent;
use Smorken\PinAuth\Shared\Contracts\Hasher;
use Smorken\PinAuth\Shared\Models\Builders\PinUserBuilder;

class PinUser extends Eloquent implements \Smorken\PinAuth\Shared\Contracts\Models\PinUser
{
    /** @use HasBuilder<PinUserBuilder<static>> */
    use HasBuilder;

    use HasFactory;

    protected static ?Hasher $hasher = null;

    protected static \Closure $hasherResolver;

    protected static string $builder = PinUserBuilder::class;

    protected $fillable = ['name', 'pin', 'external_id'];

    protected $hidden = ['pin'];

    public static function setHasherResolver(\Closure $resolver): void
    {
        self::$hasherResolver = $resolver;
    }

    public function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->name
        );
    }

    public function getAuthIdentifier(): int
    {
        return $this->getAttribute($this->getAuthIdentifierName());
    }

    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    public function getAuthPassword(): string
    {
        return $this->pin;
    }

    public function getAuthPasswordName(): string
    {
        return 'pin';
    }

    public function getRememberToken(): string
    {
        return '';
    }

    public function getRememberTokenName(): string
    {
        return '';
    }

    public function pin(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => $this->getHasher()->make($value)
        );
    }

    public function setRememberToken($value): void {}

    public function shortName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->name
        );
    }

    protected function getHasher(): Hasher
    {
        if (! self::$hasher) {
            self::$hasher = (self::$hasherResolver)();
        }

        return self::$hasher;
    }
}

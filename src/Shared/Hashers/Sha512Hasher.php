<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Hashers;

use Smorken\PinAuth\Shared\Contracts\Hasher;

class Sha512Hasher implements Hasher
{
    public function __construct(
        protected string $salt,
        protected string $hmac
    ) {}

    public function check($value, $hashedValue, array $options = []): bool
    {
        if (is_null($hashedValue) || strlen($hashedValue) === 0) {
            return false;
        }

        return hash_equals($this->make($value, $options), $hashedValue);
    }

    public function info($hashedValue): array
    {
        return [];
    }

    public function make($value, array $options = []): string
    {
        if (! $value) {
            throw new \OutOfBoundsException('A value must be provided to the hasher.');
        }

        return hash_hmac('sha512', $value.$this->salt, $this->hmac);
    }

    public function needsRehash($hashedValue, array $options = []): bool
    {
        return false;
    }
}

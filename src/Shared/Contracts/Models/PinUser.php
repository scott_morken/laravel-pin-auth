<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Contracts\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $pin
 * @property string $external_id
 *
 * Virtual
 * @property string $shortName
 * @property string $fullName
 */
interface PinUser extends Authenticatable, Model
{
    public static function setHasherResolver(\Closure $resolver): void;
}

<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Contracts;

interface Hasher extends \Illuminate\Contracts\Hashing\Hasher {}

## Laravel 10+ pin auth package

Pins are not meant to replace proper authentication.  This package exists to
provide authentication for specific routes/actions when normal authentication 
is challenging (a shared app running on a shared computer with SSO authentication).

Example: a shared computer that provides a web app that is authorized via IP. 
The IP authorization action can be protected behind the pin.

Pins are **NOT** secure as they have to use a standard hashing algorithm and not
algorithms designed for passwords.  They are subject to brute force attacks 
(and rainbow tables if you don't properly set your salt/hmac).  You've been warned!

config/auth.php
```php
...
'providers' => [
    ...
    'pin_user_provider' => [
        'driver' => 'pin_users',
    ],
]
```

config/auth.php
```php
...
'guards' => [
    ...
    'pin_user' => [
        'driver' => 'session',
        'provider' => 'pin_user_provider',
    ],
]
```

Add to `App\Http\Kernel`
```php
protected $routeMiddleware = [
    ...
    'pin.auth' => \Smorken\PinAuth\Http\Middleware\PinUserAuthenticate::class,
    ...
]
```

.env
```
PINAUTH_HASHER_SALT=randomestringforsalt
```

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

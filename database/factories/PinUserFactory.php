<?php

declare(strict_types=1);

namespace Database\Factories\Smorken\PinAuth\Shared\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\PinAuth\Shared\Models\Eloquent\PinUser;

class PinUserFactory extends Factory
{
    protected $model = PinUser::class;

    public function definition(): array
    {
        return [
            'name' => 'Foo Bar',
            'pin' => '12345678',
            'external_id' => null,
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropColumns('pin_users', ['external_id']);
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('pin_users', function (Blueprint $table) {
            $table->string('external_id', 64)->nullable();

            $table->index('external_id');
        });
    }
};
